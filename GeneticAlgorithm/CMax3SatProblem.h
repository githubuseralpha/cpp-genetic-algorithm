#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

const int ci_clause_len = 3;
const string cs_delimiter = "  ";
const string cs_line_start = "( ";
const char cc_negation = '-';

class Clause {
public:
	Clause(){};
	void addVar(int iVar, bool bIsNegative) { i_vars.push_back(iVar); b_negatives.push_back(bIsNegative);  };
	bool bCheck(vector<bool> *pvSolution);
private:
	vector<int> i_vars;
	vector<bool> b_negatives;
};

class CMax3SatProblem {
public:
	CMax3SatProblem() {};
	~CMax3SatProblem();
	void vLoad(string &psPath);
	int iCompute(vector<bool> *pvSolution);
private:
	vector<Clause *> v_clauses;
};
