﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include "GeneticAlgorithm.h"

int main(){
	srand((unsigned int)time(NULL));
	CMax3SatProblem *pc_problem = new CMax3SatProblem;
	string *ps_path = new string("C:\\Users\\Patryk\\Desktop\\max3sat\\50\\m3s_50_0.txt");
	pc_problem->vLoad(*ps_path);

	CGAOptimizer *pc_optimizer = new CGAOptimizer(50, 0.2f, 0.05f, 20);
	pc_optimizer->vInitialize(50);
	for (int i = 0; i < 10; i++) {
		pc_optimizer->vRunIteration(pc_problem);
	}
	cout << "Best Guy score = " << pc_optimizer->pcGetBestInd(pc_problem)->iFitness(pc_problem);

	delete pc_problem;
	delete pc_optimizer;
	delete ps_path;
}
