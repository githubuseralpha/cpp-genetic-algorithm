#include "CGAIndividual.h"
#include <cstdlib>
#include <ctime>

vector<CGAIndividual *> CGAIndividual::vCrossover(CGAIndividual *pcOther, float fProbability) {
	vector<CGAIndividual *> v_children;
	float f_random = float(rand()) / float((RAND_MAX));
	if (f_random < fProbability) {
		int i_cut_point = rand() % v_genotype.size();
		vector<bool> v_new_gen1;
		vector<bool> v_new_gen2;
		for (int i = 0; i < v_genotype.size(); i++) {
			if (i < i_cut_point) {
				v_new_gen1.push_back(v_genotype[i]);
				v_new_gen2.push_back(pcOther->v_genotype[i]);
			}
			else {
				v_new_gen1.push_back(pcOther->v_genotype[i]);
				v_new_gen2.push_back(v_genotype[i]);
			}
		}
		v_children.push_back(new CGAIndividual(v_new_gen1));
		v_children.push_back(new CGAIndividual(v_new_gen2));
	}
	else {
		v_children.push_back(new CGAIndividual(this->v_genotype));
		v_children.push_back(new CGAIndividual(pcOther->v_genotype));
	}
	return v_children;
}

void CGAIndividual::vMutation(float fProbability, CMax3SatProblem *pcProblem) {
	float f_random;
	for (int i = 0; i < v_genotype.size(); i++) {
		f_random = float(rand()) / float((RAND_MAX));
		if (f_random < fProbability) {
			v_genotype[i] = !v_genotype[i];
		}
	}
	for (int i = 0; i < v_genotype.size(); i++) {
		int i_result = iFitness(pcProblem);
		v_genotype[i] = !v_genotype[i];
		if(i_result > iFitness(pcProblem))
			v_genotype[i] = !v_genotype[i];
	}
}

int CGAIndividual::iFitness(CMax3SatProblem *pcProblem) {
	return pcProblem->iCompute(&v_genotype);
}

CGAIndividual::CGAIndividual(int iLen) {
	for(int i = 0; i < iLen; i++) {
		v_genotype.push_back((rand()%2));
	}
}
