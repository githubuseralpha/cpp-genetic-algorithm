#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "CMax3SatProblem.h"


using namespace std;

class CGAIndividual {
public:
	CGAIndividual(int iLen);
	~CGAIndividual() {};
	CGAIndividual(vector<bool> &vGenotype) { v_genotype = vGenotype; };
	vector<CGAIndividual *> vCrossover(CGAIndividual *pcOther, float fProbability);
	void vMutation(float fProbability, CMax3SatProblem *pcProblem);
	int iFitness(CMax3SatProblem *pcProblem);
private:
	vector<bool> v_genotype;
};

