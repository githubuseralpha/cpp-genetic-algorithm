#include "CGAOptimizer.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>

void CGAOptimizer::vInitialize(int iNumVars) {
	for (int i = 0; i < i_pop_size; i++) {
		v_population.push_back(new CGAIndividual(iNumVars));
	}
}

CGAOptimizer::CGAOptimizer(int iPopSize, float fCrossoverProb, float fMutationProb, int iTournamentSize) {
	i_pop_size = iPopSize;
	f_crossover_prob = fCrossoverProb;
	f_mutation_prob = fMutationProb;
	i_tournament_size = iTournamentSize;
}

void CGAOptimizer::vRunIteration(CMax3SatProblem *pcProblem) {
	vector<int> v_results;
	int i_best_result = 0;
	for (int i = 0; i < i_pop_size; i++) {
		int i_result = v_population[i]->iFitness(pcProblem);
		v_results.push_back(i_result);
		i_best_result = max(i_best_result, i_result);
	}
	cout << "Best result of this iteration = " << i_best_result << endl;
	vector<CGAIndividual*> v_new_pop;
	vector<CGAIndividual*> v_deletions;
	while (v_new_pop.size() < v_population.size()) {
		CGAIndividual *firstParent = chooseParent(&v_results, i_tournament_size);
		CGAIndividual *secondParent = chooseParent(&v_results, i_tournament_size);
		while(firstParent == secondParent)
			secondParent = chooseParent(&v_results, i_tournament_size);
		vector <CGAIndividual*> v_children = firstParent->vCrossover(secondParent, f_crossover_prob);
		v_children[0]->vMutation(f_mutation_prob, pcProblem);
		v_children[1]->vMutation(f_mutation_prob, pcProblem);
		v_new_pop.push_back(v_children[0]);
		v_new_pop.push_back(v_children[1]);
	}
	for (int i = 0; i < v_population.size(); i++) {
		if(v_population[i] != NULL)
			delete v_population[i];
	}
	v_population = v_new_pop;
}

CGAIndividual *CGAOptimizer::chooseParent(vector<int> *pvResults, int iTournamentSize) {
	int i_best_result = 0;
	int i_best_index = 0;
	for (int i = 0; i < iTournamentSize; i++) {
		int i_random = rand() % v_population.size();
		if ((*pvResults)[i_random] >= i_best_result) {
			i_best_result = (*pvResults)[i_random];
			i_best_index = i_random;
		}
	}
	return v_population[i_best_index];
}

CGAOptimizer::~CGAOptimizer() {
	for (int i = 0; i < v_population.size(); i++)
		if (v_population[i] != NULL)
			delete v_population[i];
};

CGAIndividual* CGAOptimizer::pcGetBestInd(CMax3SatProblem *pcProblem) {
	int i_best_result = 0;
	int i_best_index = 0;
	for (int i = 0; i < i_pop_size; i++) {
		int i_result = v_population[i]->iFitness(pcProblem);
		if (i_result > i_best_result) {
			i_best_result = i_result;
			i_best_index = i;
		}
	}
	return v_population[i_best_index];
}
