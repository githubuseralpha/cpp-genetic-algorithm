#include "CMax3SatProblem.h"
#include <fstream>
#include <algorithm>
#include <cmath>

bool Clause::bCheck(vector<bool> *pvSolution) {
	bool b_is_satisfied = false;
	for (int i = 0; i < ci_clause_len && !b_is_satisfied; i++) {
		bool b_curr_var = pvSolution->at(i_vars[i]);
		b_is_satisfied = b_negatives[i] ? !b_curr_var : b_curr_var;
	}
	return b_is_satisfied;
}

int CMax3SatProblem::iCompute(vector<bool> *pvSolution) {
	int i_result = 0;
	for (int i = 0; i < v_clauses.size(); i++) {
		if (v_clauses[i]->bCheck(pvSolution))
			i_result++;
	}
	return i_result;
}

void CMax3SatProblem::vLoad(string &psPath) {
	string s_line;
	ifstream i_file(psPath);
	if (i_file.is_open()) {
		while (getline(i_file, s_line)) {
			Clause* cClause = new Clause();
			s_line.erase(0, cs_line_start.size());
			for (int i = 0; i < ci_clause_len; i++) {
				string s_element = s_line.substr(0, s_line.find(cs_delimiter));
				cClause->addVar(abs(stoi(s_element)), s_element[0] == cc_negation);
				s_line.erase(0, s_line.find(cs_delimiter) + cs_delimiter.length());
			}
			v_clauses.push_back(cClause);
		}
		i_file.close();
	}
}

CMax3SatProblem::~CMax3SatProblem() {
	for (int i = 0; i < v_clauses.size(); i++)
		if (v_clauses[i] != NULL)
			delete v_clauses[i];
};
