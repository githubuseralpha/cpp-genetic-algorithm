#pragma once
#include "CMax3SatProblem.h"
#include "CGAIndividual.h"
#include <iostream>
#include <vector>
#include <string>

class CGAOptimizer {
public:
	CGAOptimizer(int iPopSize, float fCrossoverProb, float fMutationProb, int iTournamentSize);
	~CGAOptimizer();
	void vInitialize(int iNumVars);
	void vRunIteration(CMax3SatProblem *pcProblem);
	CGAIndividual* pcGetBestInd(CMax3SatProblem *pcProblem);
private:
	vector< CGAIndividual *> v_population;
	int i_pop_size;
	float f_crossover_prob;
	float f_mutation_prob;
	int i_tournament_size;

	CGAIndividual* chooseParent(vector<int> *pvResults, int iTournamentSize);
};
